<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class TopicRequest extends Utils
{
    public $limit;
    public $page;
    public $keyword;
    public $code;
    public $sort;
    public $price_min;
    public $price_max;

    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'topic/';
    }
}