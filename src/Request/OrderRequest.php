<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class OrderRequest extends Utils
{
    public $limit;
    public $page;
    public $keyword;
    public $start_time;
    public $end_time;
    public $order_no;
    public $out_order_no;
    public $biz_order_no;
    public $goods_code;
    public $goods_sku;
    public $goods_num;
    public $express_province;
    public $express_city;
    public $express_area;
    public $express_address;
    public $express_mobile;
    public $express_name;
    public $encrypt_plat;
    public $encrypt_name;
    public $encrypt_tel;
    public $encrypt_detail;
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'order/';
    }
}