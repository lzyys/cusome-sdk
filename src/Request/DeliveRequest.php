<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class DeliveRequest extends Utils
{
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'delive/';
    }
}