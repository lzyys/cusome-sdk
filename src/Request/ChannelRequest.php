<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class ChannelRequest extends Utils
{
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'channel/';
    }
}