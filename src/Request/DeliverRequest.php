<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class DeliverRequest extends Utils
{
    public $name;
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'deliver/';
    }
}