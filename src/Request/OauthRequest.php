<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class OauthRequest extends Utils
{
    public $limit;
    public $page;
    public $keyword;
    public $start_time;
    public $end_time;
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'oauth/';
    }

}