<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class OtherRequest extends Utils
{
    public $item_id;
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'other/';
    }
}