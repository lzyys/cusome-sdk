<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class SupplierRequest extends Utils
{
    public $id;
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'supplier/';
    }
}