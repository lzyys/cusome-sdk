<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class AftersaleRequest extends Utils
{
    public $limit;
    public $page;
    public $keyword;
    public $start_time;
    public $end_time;
    public $aftersale_no;
    public $order_no;
    public $aftersale_type;
    public $is_received;
    public $reason_type;
    public $reason;
    public $num;
    public $amount;
    public $images;
    public $address_id;
    public $company_code;
    public $company_name;
    public $express_no;
    public $biz_aftersale_no;

    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'aftersale/';
    }
}