<?php

namespace Cusome\CusomeSdk\Request;

use Cusome\CusomeSdk\Utils\Utils;

class GoodsRequest extends Utils
{
    public $limit;
    public $sort;
    public $page;
    public $keyword;
    public $cate_id;
    public $face_doudian;
    public $status;
    public $code;
    public $supplier_id;
    public $state_welfare;
    public $price_min;
    public $price_max;
    public $source;
    public $state_one_type;
    public function __construct()
    {
        parent::__construct();
        $this->prefix = 'goods/';
    }

}