<?php

namespace Cusome\CusomeSdk\Utils;

class Utils
{
    public $appid;
    public $dev = false;
    public $prefix;
    public $secret;
    public $method;
    protected $nonce;
    protected $timestamp;
    protected $url = 'https://api.cusome.net/open/api.';


    public function __construct (){
        $this->nonce = rand(10000,99999);
        $this->timestamp = time();

    }

    public function execute(){
        return self::curl(self::sign());
    }

    protected function sign(): array
    {
        $buff = "";
        $data = array_filter(get_object_vars($this));
        ksort($data);
        unset($data['method']);
        foreach ($data as $k => $v) {
            if ($k != "sign" && !is_array($v)) {
                $buff .= $k . "=" . urlencode($v) . "&";
            }
        }
        $sign = strtolower(md5(trim($buff, "&") . "&secret=" . $this->secret));
        $data['sign'] = $sign;
        return $data;
    }

    protected function curl($data){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_URL, self::getUrl().self::getMethod());
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        $content = curl_exec($curl);


        curl_close($curl);
        if(empty($content)){
            return ['code' => -10001 , 'info' => '网络请求失败'];
        }
        return json_decode($content , true);
    }

    protected function getUrl(){
        if($this->dev === true){
            $this->url = 'https://des.cusome.cn/open/api.';
        }
        return $this->url;
    }

    protected function getMethod(){
        return $this->prefix . $this->method;
    }



}